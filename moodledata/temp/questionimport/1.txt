::1:: I have four sides and all sides are equal, Who am I ?
{
~ Rectangle
=Square
~ Triangle
~ None of these
}


 
::1:: I have four sides, only opposite sides are equal. Who am I ?
{
=Rectangle
~ Square
~ Triangle
~ None of these
}


 
::1:: I have three sides and they can be equal or unequal, Who am I ?
{
~ Rectangle
~ Square
=Triangle
~ None of these
}


 
::1:: I am unique, I am round, no corners, Who am I ?
{
~ Rectangle
=Circle
~ Triangle
~ Square
}


 
::1:: What shape is your ruler ?
{
=Rectangle
~ Circle
~ Triangle
~ Square
}

 
::1:: How many faces does a Cone has ?
{
~ 1
=2
~ 3
~ 4
}


 
::1:: How many edges does a Cone has ?
{
=1
~ 2
~ 3
~ 4
}


 
::1:: The two shapes which have no corner ?
{
~ Cylinder and cone
~ Cylinder and Sphere
=Sphere and cone
~ Sphere and cube
}


 
::1:: Shape which has one edge and one vertex ?
{
=Cone
~ Cylinder
~ Cube
~ Cuboid
}


 
::1:: Which two shapes have same number of Corners.
{
~ Cube and Cone
~ Cube and Cylinder
=Cube and Cuboid
~ None of these
}

::1:: Which shape has only 1 Face, no Corner, no Edges.
{
~ Cube
~ Cone
=Sphere
~ one of these
}
 
 ::1:: The shape of a dice is a Cube, True or False ?
{
=TRUE
~ FALSE
~
~
}


 
::1:: The shape of a ball is a Cone, True or False ?
{
~ TRUE
=FALSE
~
~
}


::1:: A Circle has 4 sides, True or False ?
{
~ TRUE
=FALSE
~
~
}


 
::1:: A Triangle has only straight lines, True or False ?
{
=TRUE
~ FALSE
~
~
}

 
::1:: Ovals and Circles have only curved lines, True or False ?
{
=TRUE
~ FALSE
~
~
}


 
::1:: A Semi-circle is half of a circle, True or False ?
{
=TRUE
~ FALSE
~
~
}


 
::1:: A Square has 4 sides, True or False ?
{
=TRUE
~ FALSE
~
~
}


 
::1:: A rectangle has 4 sides, True or False ?
{
=TRUE
~ FALSE
~
~
}


 
::1:: A triangle has 4 sides, True or False ?
{
~ TRUE
=FALSE
~
~
}