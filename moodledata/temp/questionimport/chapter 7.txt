::1:: A wheat plant belongs to the category of
{
=herb    
~shrub
~tree                 
~None of these
}

::1:: Classify Neem in one of the categories of plants
{
~Herb                           
~hrub
=Tree                            
~All of these
}

::1:: A plant has weak stem and cannot stand upright. It spreads on the ground. It may be called
{
~herb                
~shrub
=creeper
~climber
}

::1:: The plants having weak stem and take support on neighbouring structures is called a
{
~creeper            
=climber
~Both of these               
~None of these
}

::1:: In which of the following plants the stem is very weak and needs support?
{
~Gourd                         
~Cucurbita
=Both of these   
~None of these
}

::1:: In plants of gourd and cucurbita, some thread like structures arise from the stem that helps the plants to climb. These structures are called
{
~leaf tendrils    
~thorns
~spines             
=stem tendrils
}

::1:: In a mango tree the branches appear
{
~at the base of stem
=higher up on the stem
~no where on the stem
~None of the above
}

::1:: Lemon tree belongs to which category?
{
~Herb                           
=Shrub
~Tree                            
~Can't say
}

::1:: A tomato plant has stems of which colour?
{
=Green
~Red
~Sometimes green, sometimes red
~Neither green nor red
}

::1:: The stem of tomato plant is ____.
{
=tender             
~thick
~hard                
~t~hin
}

::1:: Water moves up in the stem and goes to which part of plant?
{
~Stem
~Leaves
~Flowers
=Plant parts attached to stem
}


::1:: The part of the leaf by which it is attached to the stem is called
{
=petiole 
~lamina
~veins   
~None of these
}

::1:: The lines on the broad green part of the leaf are called
{
~lamina
=veins
~venation          
~None of these
}

::1:: The water is released
{
~by plants into air by process of transpiration
~through leaves in the form of water vapour
=Both the above
~None of the above
}

::1:: The food produced in the leaves is in the form of
{
=starch              
~proteins
~chlorophyll                  
~A~ll of these
}

::1:: Which part of the plant anchors the plant firmly in the soil?
{
=Root                
~Stem
~Both of these  
~None of these
}


::1:: The plants having leaves with parallel venation have ____.
{
~taproots          
~lateral roots
=fibrous roots 
~None of these
}


::1:: Select the plant whose roots store food for future use.
{
~Turnip                         
~Sweet potato
~Tapioca                      
=All of these
}

::1:: Petals are
{
~modified leaf
~usually coloured
~forms part of corolla of flower
=All of these
}


::1:: Pistil is the
{
~inner most part of the flower
~female structure of the flower
=Both the above
~None of the above
}
